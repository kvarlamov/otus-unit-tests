﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.WebHost;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    //allow run common methods for test classes once
    //for example IOC could be here
    public class TestFixtireInMemory : IDisposable
    {
        public static readonly Guid PartnerId = Guid.NewGuid();
        public DbContextOptions<DataContext> DbOptions { get; private set; }
        public IServiceProvider ServiceProvider { get; set; }
        public IServiceCollection ServiceCollection { get; set; }
        
        public TestFixtireInMemory()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var configuration = configurationBuilder.Build();
            ServiceCollection = new ServiceCollection();
            new Startup(configuration).ConfigureServices(ServiceCollection);
            //var serviceProvider = serviceCollection.BuildServiceProvider();

            var serviceProvider = GetServiceProvider();
            ServiceProvider = serviceProvider;

            //GetInMemoryContextWayOne();
        }

        private IServiceProvider GetServiceProvider()
        {
            var serviceProvider = ServiceCollection
                .ConfigureInMemoryContext()
                .BuildServiceProvider();
            SeedData(serviceProvider);

            return serviceProvider;
        }

        private void GetInMemoryContextWayOne()
        {
            var dbOptions = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase("SetPartnerPromoCodeLimitTest")
                .UseLazyLoadingProxies()
                .ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;

            DbOptions = dbOptions;

            using var context = new DataContext(dbOptions);

            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            context.AddRange(SeedData());
            
            context.SaveChanges();
        }

        private void SeedData(IServiceProvider serviceProvider)
        {
            using var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var context = scope.ServiceProvider.GetService<DataContext>();
            
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            context.AddRange(SeedData());
            
            context.SaveChanges();
        }

        private IEnumerable<Partner> SeedData()
        {
            return new List<Partner>()
            {
                new Partner()
                {
                    Id = PartnerId,
                    Name = "Test Partner",
                    IsActive = true,
                    NumberIssuedPromoCodes = 5,
                    PartnerLimits = new List<PartnerPromoCodeLimit>()
                    {
                        new PartnerPromoCodeLimit()
                        {
                            Id = Guid.Parse("c9bef066-3c5a-4e5d-9cff-bd54479f075e"),
                            CreateDate = new DateTime(2020,05,3),
                            EndDate = new DateTime(2020,10,15),
                            Limit = 1000 
                        }
                    }
                }
            };
        }

        public void Dispose()
        {
        }
    }

    public static class Configuration
    {
        public static IServiceCollection ConfigureInMemoryContext(this IServiceCollection services)
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            services.AddDbContext<DataContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDb", builder => { });
                options.UseInternalServiceProvider(serviceProvider);
            });

            services.AddTransient<DbContext, DataContext>();

            return services;
        }
    }
}