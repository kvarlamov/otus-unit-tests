﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using FluentAssertions.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<TestFixtireInMemory>
    {
        private readonly Mock<IRepository<Partner>> _partnerRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly DateTime _endDateExpired = DateTime.Parse("2022-04-01");
        private readonly DateTime _endDateNotExpired = DateTime.Now.AddYears(5);
        private readonly DbContextOptions<DataContext> _dbContextOptions;
        private readonly IServiceProvider _serviceProvider;
        public SetPartnerPromoCodeLimitAsyncTests(TestFixtireInMemory testFixtireInMemory)
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnerRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
            _dbContextOptions = testFixtireInMemory.DbOptions;
            _serviceProvider = testFixtireInMemory.ServiceProvider;
        }
        
        [Fact]
        public async Task SetLimit_PartnerNotFound_Return404()
        {
            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Partner)null);
            SetPartnerPromoCodeLimitRequest request = GetPartnerPromoCodeLimitRequest();

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }

        [Fact]
        public async Task SetLimit_PartnerBlocked_Return400()
        {
            var request = GetPartnerPromoCodeLimitRequest();
            var partner = new PartnerBuilder().Build();
            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            result.Should().BeOfType<BadRequestObjectResult>()
                .And.Match<BadRequestObjectResult>(x =>
                    x.StatusCode == 400 &&
                    x.Value.Equals("Данный партнер не активен"));
        }

        [Fact]
        public async Task SetLimit_NewZeroLimitRequest_BadRequest()
        {
            var request = GetPartnerPromoCodeLimitRequest(0);
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate).Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = new PartnerBuilder()
                .MakeActive()
                .SetNumberIssuedPromoCodes(5)
                .SetPartnerPromoCodeLimit(partnerLimits)
                .Build();
            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            result.Should().BeOfType<BadRequestObjectResult>()
                .And.Match<BadRequestObjectResult>(x =>
                    x.StatusCode == 400 &&
                    x.Value.Equals("Лимит должен быть больше 0"));
        }

        [Fact]
        public async Task SetLimit_NewZeroLimitRequest_StateIsNotChanged()
        {
            var request = GetPartnerPromoCodeLimitRequest(0);
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate).Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = new PartnerBuilder()
                .MakeActive()
                .SetNumberIssuedPromoCodes(5)
                .SetPartnerPromoCodeLimit(partnerLimits)
                .Build();
            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);
            
            _partnerRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Never);
        }

        [Fact]
        public async Task SetLimit_NewZeroLimitRequest_NewLimitNotAdded()
        {
            var request = GetPartnerPromoCodeLimitRequest(0);
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate).Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = new PartnerBuilder()
                .MakeActive()
                .SetNumberIssuedPromoCodes(5)
                .SetPartnerPromoCodeLimit(partnerLimits)
                .Build();
            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);
            
            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            partner.PartnerLimits.Count.Should().Be(partnerLimits.Length);
        } 

        [Fact]
        public async Task SetLimit_AddNewLimitWhenOldLimitCancelDateIsNull_OldLimitHasCancelDate()
        {
            var request = GetPartnerPromoCodeLimitRequest();
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate)
                .Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = new PartnerBuilder()
                .MakeActive()
                .SetNumberIssuedPromoCodes(5)
                .SetPartnerPromoCodeLimit(partnerLimits)
                .Build();
            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            partnerLimit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async Task SetLimit_AddNewLimitWhenOldLimitCancelDateIsNull_NumberIssuedPromoCodesSetZero()
        {
            var request = GetPartnerPromoCodeLimitRequest();
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate)
                .Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = new PartnerBuilder()
                .MakeActive()
                .SetNumberIssuedPromoCodes(5)
                .SetPartnerPromoCodeLimit(partnerLimits)
                .Build();
            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        
        [Fact]
        public async Task SetLimit_AddNewLimitWhenOldLimitEndDateNotExpired_OldLimitHasNewCancelDate()
        {
            var request = GetPartnerPromoCodeLimitRequest();
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>()
                .With(p => p.EndDate, _endDateNotExpired)
                .Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = new PartnerBuilder()
                .MakeActive()
                .SetNumberIssuedPromoCodes(5)
                .SetPartnerPromoCodeLimit(partnerLimits)
                .Build();
            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            partnerLimit.CancelDate.Value.Day.Should().Be(DateTime.Now.Day);
        }
        
        [Fact]
        public async Task SetLimit_AddNewLimitWhenOldLimitEndDateNotExpired_NumberIssuedPromoCodesSetZero()
        {
            var request = GetPartnerPromoCodeLimitRequest();
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>()
                .With(p => p.EndDate, _endDateNotExpired)
                .Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = new PartnerBuilder()
                .MakeActive()
                .SetNumberIssuedPromoCodes(5)
                .SetPartnerPromoCodeLimit(partnerLimits)
                .Build();
            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async Task SetLimit_AddNewLimitWhenOldLimitEndDateExpired_NumberIssuedPromoCodesNotChanged()
        {
            var request = GetPartnerPromoCodeLimitRequest();
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>()
                .Without(p => p.Partner)
                .With(p => p.EndDate, _endDateExpired)
                .Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = new PartnerBuilder()
                .MakeActive()
                .SetNumberIssuedPromoCodes(5)
                .SetPartnerPromoCodeLimit(partnerLimits)
                .Build();
            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            partner.NumberIssuedPromoCodes.Should().Be(5);
        }
        
        [Fact]
        public async Task SetLimit_AddNewLimitWhenOldCancelDateNotNull_NumberIssuedPromoCodesNotChanged()
        {
            const int numberIssuePromoCodes = 5;
            
            var request = GetPartnerPromoCodeLimitRequest(0);
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>()
                .Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = new PartnerBuilder()
                .MakeActive()
                .SetNumberIssuedPromoCodes(numberIssuePromoCodes)
                .SetPartnerPromoCodeLimit(partnerLimits)
                .Build();
            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            partner.NumberIssuedPromoCodes.Should().Be(numberIssuePromoCodes);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("2022-04-01")]
        public async Task SetLimit_NewLimit_OkResultWithStatusCode201(string cancelDateStr)
        {
            DateTime? cancelDate = null;
            if (!string.IsNullOrEmpty(cancelDateStr))
                cancelDate = DateTime.Parse(cancelDateStr);
            
            var request = GetPartnerPromoCodeLimitRequest();
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>()
                .With(p => p.CancelDate, cancelDate)
                .Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = new PartnerBuilder()
                .MakeActive()
                .SetNumberIssuedPromoCodes(5)
                .SetPartnerPromoCodeLimit(partnerLimits)
                .Build();
            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            result.Should().BeOfType<CreatedAtActionResult>().Which.StatusCode.Should().Be(201);
        }

        [Fact]
        public async Task SetLimit_NewLimit_CallsGetAndUpdateOnce()
        {
            var request = GetPartnerPromoCodeLimitRequest();
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate)
                .Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = new PartnerBuilder()
                .MakeActive()
                .SetNumberIssuedPromoCodes(5)
                .SetPartnerPromoCodeLimit(partnerLimits)
                .Build();
            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);
            
            _partnerRepositoryMock.Verify(x => x.GetByIdAsync(It.IsAny<Guid>()), Times.Once);
            _partnerRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Once);
        }

        [Fact]
        public async Task SetLimit_NewLimit_SavedToDatabase()
        {
            var repository = _serviceProvider.GetService<IRepository<Partner>>();

            //var context = new DataContext(_dbContextOptions);
            //var repository = new EfRepository<Partner>(context);
            var partnerController = new PartnersController(repository);
            var request = GetPartnerPromoCodeLimitRequest();
            
            await partnerController.SetPartnerPromoCodeLimitAsync(TestFixtireInMemory.PartnerId, request);

            //var partner = context.Partners.First(x => x.Id == TestFixtireInMemory.PartnerId);
            var partner = await repository.GetByIdAsync(TestFixtireInMemory.PartnerId);
            
            partner.PartnerLimits.Count.Should().Be(2);
            partner.PartnerLimits.Last().Should()
                .Match<PartnerPromoCodeLimit>(x =>
                    x.Limit == request.Limit && x.CancelDate == null && x.EndDate == request.EndDate);
        }

        private SetPartnerPromoCodeLimitRequest GetPartnerPromoCodeLimitRequest(int limit=1)
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                Limit = limit,
                EndDate = _endDateExpired
            };
        }
    }
}