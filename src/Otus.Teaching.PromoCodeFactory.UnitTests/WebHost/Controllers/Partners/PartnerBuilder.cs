﻿using System;
using System.Collections.Generic;
using AutoFixture;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerBuilder
    {
        private string _name;
        private int _numberIssuedPromoCodes;
        private bool _isActive;
        private List<PartnerPromoCodeLimit> _partnerLimits;

        public PartnerBuilder()
        {
            _partnerLimits = new List<PartnerPromoCodeLimit>();
        }

        public PartnerBuilder MakeActive()
        {
            _isActive = true;
            return this;
        }

        public PartnerBuilder SetNumberIssuedPromoCodes(int number)
        {
            _numberIssuedPromoCodes = number;
            return this;
        }

        public PartnerBuilder SetPartnerPromoCodeLimit(IEnumerable<PartnerPromoCodeLimit> partnerLimits)
        {
            _partnerLimits.AddRange(partnerLimits);

            return this;
        }
        
        
        public Partner Build()
        {
            return new Partner()
            {
                Id = Guid.NewGuid(),
                IsActive = _isActive,
                Name = _name,
                NumberIssuedPromoCodes = _numberIssuedPromoCodes,
                PartnerLimits = _partnerLimits
            };
        }
    }
}